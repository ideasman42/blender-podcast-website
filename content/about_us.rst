
********
About us
********

*"We want a Blender Podcast"*, a sentence which you saw quite often in various web forums.
The Blender community is growing and growing and it’s time for more audio personality!

So with this "old" wish of the community in mind, Thomas Dinges started ``blender-podcast.org``
in June 2011 together with Campbell Barton.

Blender Podcast is a platform for all Blender people, out there.
We have cool guests on the show, both artists and developers, freelancers and studios.
If you’re into 3d graphics, animation, open-source and of course anything blender related,
there should be something in it for you!

Blender Podcast logo designed by iKlsR.

Podcast music composed by `Joram Letwory <http://joramletwory.com>`__.

You can visit us in IRC on Freenode inside the channel ``#blender-podcast``

The Blender Podcast is part of the DingTo Network.


Hosts
=====

.. list-table::

   * - .. figure:: /images/mugshot_thomas.jpg

          `Thomas Dinges <http://dingto.org/>`__

     - .. figure:: /images/mugshot_campbell.jpg

          Campbell Barton
