******************************************
Episode 25 / Interview with Dolf Veenvliet
******************************************

Hi,
in this episode we interviewed Dolf Veenvliet,
who just released a great new training DVD about 3D printing. He was also part of the Sintel Open Movie.

Campbell and I also talked about the upcoming Blender 2.68 release and this years Google summer of code.

– Thomas

.. admonition:: Listen
   :class: episode

   .. audio:: Blender_Podcast_Episode_025

   :Length: 1:04:51
   :Recorded: 21 June, 2013
   :Download:
      :episode:`OGG <Blender_Podcast_Episode_025.ogg>`
      :episode:`MP3 <Blender_Podcast_Episode_025.mp3>`

.. seealso::

   - `Dolfs website <http://www.macouno.com>`__
   - `Blender for 3D Printing DVD <http://www.blender3d.org/e-shop/product_info_n.php?products_id=160>`__
   - `Shapewright <http://shapewright.com>`__
   - `Shapeways <http://www.shapeways.com>`__
   - `Thingiverse <http://www.thingiverse.com>`__
   - `Thomas’s GSOC Wiki <http://wiki.blender.org/index.php/User:DingTo/GSoC_2013>`__
   - `Blender/StackExchange (ask questions!) <http://blender.stackexchange.com>`__
   - `Modeling Mailing List (mailing list to discuss modeling tool development) <http://lists.blender.org/mailman/listinfo/bf-modeling>`__
   - `Radiance Renderer (referenced in relation to lighting) <http://www.radiance-online.org>`__
