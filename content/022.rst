
.. figure:: /images/022_law_of_needs.png
   :align: right
   :width: 300px


**********************************
Episode 22 / Upcoming 2.66 Release
**********************************

Campbell and I talked about the upcoming Blender 2.66 release and new features.
Campbell also presents “Campbells Law of Features”. Have fun!

– Thomas

.. admonition:: Listen
   :class: episode

   .. audio:: Blender_Podcast_Episode_022

   :Length: 00:52:43
   :Recorded: 16 February, 2013
   :Download:
      :episode:`OGG <Blender_Podcast_Episode_022.ogg>`
      :episode:`MP3 <Blender_Podcast_Episode_022.mp3>`

.. seealso::

   - `Release logs of the upcoming Blender 2.66 <http://wiki.blender.org/index.php/Dev:Ref/Release_Notes/2.66>`__
   - `Global Game Jam – “Little Red Hiding Hood” <http://2013.globalgamejam.org/2013/little-red-hiding-hood>`__
   - `“Saalschutz” music video <https://www.youtube.com/watch?v=rtwOZdCUu1s&feature=youtu.be>`__
   - `Non-Progressive integrator – dingto.org <http://dingto.org/?p=690>`__
