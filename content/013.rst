*************************************
Episode 13 / Blender Development News
*************************************

Welcome to the first episode in 2012!
We discussed some interesting development news, and talked about recent discussions on the mailing list,
including the Interface and Collada. We also talk about what we have been up to the last couple of weeks.

– Thomas

.. admonition:: Listen
   :class: episode

   .. audio:: Blender_Podcast_Episode_013

   :Length: 00:45:00
   :Recorded: 23 January, 2012
   :Download:
      :episode:`OGG <Blender_Podcast_Episode_013.ogg>`
      :episode:`MP3 <Blender_Podcast_Episode_013.mp3>`

.. seealso::

   - `Mailing list, Collada Thread <http://lists.blender.org/pipermail/bf-committers/2012-January/034972.html>`__
   - `Mailing list, User Interface Thread <http://lists.blender.org/pipermail/bf-committers/2012-January/035213.html>`__
   - `Cleaning up the Blender UI Proposal by William Reynish <http://billrey.blogspot.com/2011/10/cleaning-up-blender-ui.html>`__
   - `Thomas’ new film project, Gemini <http://gemini.dingto.org>`__
   - `Gloabl Game Jam <http://globalgamejam.org>`__
