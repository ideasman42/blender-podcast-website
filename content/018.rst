
.. figure:: /images/018_ian_hubert.jpg
   :align: right


**************************************
Episode 18 / Interview with Ian Hubert
**************************************

Hey,
Campbell had the chance to interview Ian Hubert, the director of the Mango project (and Project London of course) in Amsterdam.
Have fun!

– Thomas

.. admonition:: Listen
   :class: episode

   .. audio:: Blender_Podcast_Episode_018

   :Length: 1:00:06
   :Recorded: 18 June, 2012
   :Download:
      :episode:`OGG <Blender_Podcast_Episode_018.ogg>`
      :episode:`MP3 <Blender_Podcast_Episode_018.mp3>`

.. seealso::

   - `Mango Open Movie <https://mango.blender.org>`__
   - `Project London <http://projectlondonmovie.com>`__
   - `Robot Soup <http://www.robotsoup.com>`__
   - `Color Hug – Open source color management hardware <http://www.hughski.com>`__
   - `Color Hug – Interview on Linux Outlaws (239) <http://sixgun.org/episodes/lo239>`__
   - `Mango Color Space Notes (By Brecht) <http://wiki.blender.org/index.php/Org:Institute/Open_projects/Mango/ColorSpaceNotes>`__
