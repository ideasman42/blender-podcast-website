***************************************
Episode 24 / Blender 2.67, FMX and more
***************************************

Hi,
in this episode we talked about the new Blender 2.67 release, the FMX conference in Germany, 3D printing addon
and the blenderartists forum.

– Thomas

.. admonition:: Listen
   :class: episode

   .. audio:: Blender_Podcast_Episode_024

   :Length: 00:45:46
   :Recorded: 1 May, 2013
   :Download:
      :episode:`OGG <Blender_Podcast_Episode_024.ogg>`
      :episode:`MP3 <Blender_Podcast_Episode_024.mp3>`

.. seealso::

   - `Blender 2.67 release <http://wiki.blender.org/index.php/Dev:Ref/Release_Notes/2.67>`__
   - `FMX conference <http://www.fmx.de>`__
   - `Artist “zuggamasta” with his 365 project <http://zuggamasta.de>`__
