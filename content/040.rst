
*****************************
Episode 40 / Bconf interviews
*****************************

Hi,

End of October I visited Amsterdam for the Blender conference. I interviewed several people there,
and Campbell and I also discuss what we've been up to.

| Have fun!
| ~ Thomas

:00\:00: Intro
:00\:45: Lukas Stockner
:03\:12: Daniel Martínez Lara
:08\:30: Alexander Mitzkus
:12\:02: Lino Thomas
:13\:49: Julian Eisel
:17\:26: Sergey Sharybin
:28\:04: Campbell and me again :D

.. admonition:: Listen
   :class: episode

   .. audio:: Blender_Podcast_Episode_040

   :Length: 56:25
   :Recorded: 9 November, 2015
   :Download:
      :episode:`OGG <Blender_Podcast_Episode_040.ogg>`
      :episode:`MP3 <Blender_Podcast_Episode_040.mp3>`

.. seealso::

   - `Blender Conference <http://www.blender.org/conference/>`__
