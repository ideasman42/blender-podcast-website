%%%%%%%%%%%%%%%%%%
  Blender Podcast
%%%%%%%%%%%%%%%%%%

Welcome to the Blender Podcast, by Campbell Barton and Thomas Dinges.

Subscribe on `iTunes <https://itunes.apple.com/de/podcast/blender-podcast/id451042520>`__ or follow
us on `Twitter <https://www.twitter.com/BlenderPodcast>`__.


##############
Latest Episode
##############

.. always keep latest episode here

.. include:: 041.rst

----

********
Episodes
********

.. toctree::
   :maxdepth: 1

   041.rst
   040.rst
   039.rst
   038.rst
   037.rst
   036.rst
   035.rst
   034.rst
   033.rst
   032.rst
   031.rst
   030.rst
   029.rst
   028.rst
   027.rst
   026.rst
   025.rst
   024.rst
   023.rst
   022.rst
   021.rst
   020.rst
   019.rst
   018.rst
   017.rst
   016.rst
   015.rst
   014.rst
   013.rst
   012.rst
   011.rst
   010.rst
   009.rst
   008.rst
   007.rst
   006.rst
   005.rst
   004.rst
   003.rst
   002.rst
   001.rst


***********
Other Pages
***********

.. toctree::
   :maxdepth: 1

   about_us.rst
