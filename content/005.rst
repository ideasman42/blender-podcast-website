*****************************************************
Episode 5 / Game Engine, Donations & Building Blender
*****************************************************

Heres the fifth episode of the Blender Podcast.
Campbell and I talk about some current development stuff, like the future of the Game Engine,
new donation possibilities and for whom it’s useful to compile your own Blender version.

.. admonition:: Listen
   :class: episode

   .. audio:: Blender_Podcast_Episode_005

   :Length: 00:48:40
   :Recorded: 7 August, 2011
   :Download:
      :episode:`OGG <Blender_Podcast_Episode_005.ogg>`
      :episode:`MP3 <Blender_Podcast_Episode_005.mp3>`

.. seealso::

   - `Game Engine “Where is the development?” thread <http://blenderartists.org/forum/showthread.php?226524-Where-is-the-development>`__
   - `48hours Game Contest <http://archive.globalgamejam.org/2011/whip-frog>`__
   - `Funding & Donations <http://code.blender.org/2011/08/blender-development-fund-fund-raisers/>`__
   - `Compile Blender <http://wiki.blender.org/index.php/Dev:Doc/Building_Blender>`__
