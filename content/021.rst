*******************************************
Episode 21 / Interview with Francesco Siddi
*******************************************

Campbell and I interviewed Francesco Siddi, known from his work on Tears of Steel and we also talked about
recent developments and plans for the upcoming year.
Merry Christmas to you all and a good start into 2013!

– Thomas

.. admonition:: Listen
   :class: episode

   .. audio:: Blender_Podcast_Episode_021

   :Length: 1:10:34
   :Recorded: 19 / 23 December, 2012
   :Download:
      :episode:`OGG <Blender_Podcast_Episode_021.ogg>`
      :episode:`MP3 <Blender_Podcast_Episode_021.mp3>`

.. seealso::

   - `Website of Francesco <http://www.fsiddi.com>`__
   - `Caminandes – Short Movie <http://www.caminandes.com>`__
   - `Release logs of the upcoming Blender 2.66 <http://wiki.blender.org/index.php/Dev:Ref/Release_Notes/2.66>`__
   - `New OSL website <http://www.openshading.com>`__
   - `BlenderDay conference <https://blenderday.de>`__
   - `The Gentoo guy <https://blog.flameeyes.eu/2012/12/bloody-upstream#gsc.tab=0>`__
