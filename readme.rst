
**********************
Blender Podcast Source
**********************

This is the source files for the ``blender-podcast.org`` web site.

It may be of interest to anyone looking to setup a podcast using static generated HTML.

- Uses Sphinx to generate the website.
- A simple extension to embed audio. (see: ``exts/audio.py``),
  written for this site.
- Minor ``css`` edits to remove Python specific text.
