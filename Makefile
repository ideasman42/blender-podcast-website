
SPHINXOPTS    =
PAPER         =
SPHINXBUILD   = sphinx-build
BUILDDIR      = build


# os specific
ifeq ($(OS), Darwin)
	# OSX
	OPEN_CMD="open"
else
	# Linux/FreeBSD
	OPEN_CMD="xdg-open"
endif

all: FORCE
	$(SPHINXBUILD) -b html $(SPHINXOPTS) ./content "$(BUILDDIR)/html"
	@echo "To view, run:"
	@echo "  " $(OPEN_CMD) " \"$(BUILDDIR)/html/index.html\""

clean: FORCE
	rm -rf "$(BUILDDIR)/html" "$(BUILDDIR)/latex"

# TODO, use real location
upload: all FORCE
	rsync --progress -ave "ssh -p 22" $(BUILDDIR)/html/* ideasman42@download.blender.org:/data/ftp/ideasman42/podcast

FORCE:
